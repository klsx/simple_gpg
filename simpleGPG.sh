#!/bin/bash

#Text effects
GREEN="$(echo -e "\e[32m")"
RED="$(echo -e "\e[31m")"
BOLD="$(echo -e "\e[1m")"
RESET="$(echo -e "\e[0m")"


function genKeys {
	clear
	printf "${BOLD}---- Generation of the keys ----$RESET \n"
	read -p "Enter your name : " name
	read -p "Enter your email address : " email
	printf "Please specify how long the key should be valid.
	  0 = key does not expire
	  <n>  = key expires in n days
	  <n>w = key expires in n weeks
	  <n>m = key expires in n months
	  <n>y = key expires in n years\n"
	read -p "Enter the keys's duration : " duration
	echo -n "Enter your passphrase: "
	read -s pass
cat > .tempGenFile <<EOF
	Key-Type: RSA
	Key-Length: 2048
	Subkey-Type: RSA
	Subkey-Length: 2048
	Name-Real: ${name}
	Name-Email: ${email}
	Expire-Date: ${duration} 
	Passphrase: ${pass}
EOF

	printf "\n$RED"
	gpg --batch --gen-key .tempGenFile
	if [[ $? -eq 0 ]] ; then
		printf "$RESET"
		rm .tempGenFile
		gpg --output ./"${email}".key --armor --export ${email}
		gpg --sign-key ${email}
		printf "${GREEN}Public key path : $(pwd)/${email}.key $RESET\n"
	else
		rm .tempGenFile
		printf "$RESET"
		exit
	fi
}

function loadPublicKey {
	clear
	printf "$BOLD---- Load a public key from a file ----$RESET \n"
	read -e -p "Enter path to import a public key : " path
	gpg --import $path
}

function encryptFile {
	clear
	printf "$BOLD---- Encrypt a file ----$RESET \n"
	read -e -p "Enter the path of the file you want to encrypt: " inputPath
	read -e -p "Enter the path and the name of the output encrypted file: " outputPath
	read -p "Enter the receiver address: " receiver
	read -p "Do you want to sign the encrypted file ? (Y/n) " signPrompt

	firstCharSignPrompt=${signPrompt:0:1}
	if [[ "${firstCharSignPrompt^h}" == "n" ]] ; then
		gpg --encrypt --armor --output ${outputPath} --recipient "${receiver}" ${inputPath}
		printf "\nEncrypting without signing the file\n"
		printf "${GREEN}Path to your encrypted file : $outputPath $RESET\n"
	else
		gpg --encrypt --sign --armor --output ${outputPath} --recipient "${receiver}" ${inputPath}
		printf "\nEncrypting with signing the file\n"
		printf "${GREEN}Path to your encrypted file : $outputPath $RESET\n"
	fi
}

function decryptFile {
	clear
	printf "$BOLD---- Decrypt a file ----$RESET \n"
	read -e -p "Enter the path of the file you want to decrypt: " inputPath
	read -e -p "Enter the path and the name of the decrypted file: " outputPath

	gpg --decrypt ${inputPath} > ${outputPath}
	printf "$GREEN Path to your decrypted file : $outputPath $RESET \n"
}

function verifyFile {
	clear
	printf "$BOLD---- Verify a file ----$RESET \n"
	read -e -p "Enter the path of the file you want to verify: " inputPath
	gpg --verify $inputPath &>/dev/null
	if [[ $? -eq 0 ]] ; then
		printf "The file is not encrypted\n"
		gpg --verify $inputPath
	else
		printf "The file is encrypted or corrupted\n"
		gpg --decrypt $inputPath
	fi
	
}

function signFile {
	clear
	printf "$BOLD---- Sign a file ----$RESET \n"
	read -e -p "Enter the path of the file you want to sign: " inputPath
	read -e -p "Enter the path and the name of the output file: " outputPath
	read -p "Enter the receiver address: " receiver
	gpg --recipient $receiver --sign --armor --output $outputPath $inputPath 

}

function main {
	clear
	printf "$BOLD---- Simple GPG Tool ----$RESET \n"
	printf "1) Generate yours keys (public/private)\n"
	printf "2) Load a public key\n"
	printf "3) Encrypt a file\n"
	printf "4) Decrypt a file\n"
	printf "5) Verify a file\n"
	printf "6) Sign-only a file\n"

	read -p "Choice > " input

	if [[ "${input}" -eq 1 ]] ; then
		genKeys
	elif [[ "${input}" -eq 2 ]] ; then
		loadPublicKey
	elif [[ "${input}" -eq 3 ]] ; then
		encryptFile
	elif [[ "${input}" -eq 4 ]] ; then
		decryptFile
	elif [[ "${input}" -eq 5 ]] ; then
		verifyFile
	elif [[ "${input}" -eq 6 ]] ; then
		signFile
	fi
}

if  ! command -v gpg &> /dev/null
then
	printf "$RED You need to install the ${BOLD}gnupg2${RESET}${RED} package $RESET\n"
	exit
else
	main
fi